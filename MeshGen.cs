﻿/* MeshGen.cs
 * Copyright Eddie Cameron 2014
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof( MeshFilter ) )]
[RequireComponent( typeof( MeshRenderer ) )]
public abstract class MeshGen : BehaviourBase {
    public bool useCollider;
    public bool markDynamic = false;

    protected Mesh mesh { get; private set; }

    MeshCollider meshCollider;
    MeshFilter meshFilter;
    bool colliderToUpdate;

    protected override void Awake()
    {
        base.Awake();

        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        mesh = meshFilter.mesh;
    }

    public void GenMesh()
    {
        Vector3[] vertices = null;
        Vector2[] uvs = null;
        int[] triangles = null;

        GeneratePoints( ref vertices, ref uvs, ref triangles );

        GenMesh( vertices, uvs, triangles );
    }

    protected void GenMesh( Vector3[] vertices, Vector2[] uvs, int[] triangles ) {

        mesh = mesh ?? new Mesh();
        if ( markDynamic )
            mesh.MarkDynamic();
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        AddNormals( mesh, vertices, triangles );

        meshFilter.mesh = mesh;
        colliderToUpdate = true;
        OnMeshGenerated();
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();

        if ( colliderToUpdate && useCollider ) {
            meshCollider.sharedMesh = null;
            meshCollider.sharedMesh = mesh;
        }
    }

    protected abstract void GeneratePoints(
        ref Vector3[] vertices,
        ref Vector2[] uvs,
        ref int[] triangles );

    protected virtual void OnMeshGenerated() { }

    public virtual void AddNormals( Mesh mesh, Vector3[] vertices, int[] triangles ) {
        mesh.RecalculateNormals();
    }
}