﻿/* ScrollGroundGen.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScrollGroundGen : BehaviourBase {
    public float perlinScale = .1f;
    public float maxGroundHeight;
    public float scrollSpeed = 2;
    public float trackWidth = 3f;

    public PlaneGen segmentObject;

    PlaneGen[] segments;
    float[] vertexHeights;
    int frontSegment;
    float distTravelled;
    int segmentSwitches;

    protected override void Start() {
        base.Start();
        InitSegments();
    }

    protected override void Update() {
        base.Update();

        float scrollDist = scrollSpeed * Time.deltaTime;
        foreach ( var segment in segments ) {
            segment.transform.Translate( Vector3.back * scrollDist );
        }

        distTravelled += scrollDist;
        if ( (int)( distTravelled / segmentObject.zSize ) > segmentSwitches ) {
            // switch front segment to back
            segments[frontSegment].transform.Translate( Vector3.forward * segmentObject.zSize * 3 );
            UpdateMeshForDistance( segments[frontSegment] );
            frontSegment = ( frontSegment + 1 ) % 3;
            segmentSwitches++;
        }
    }

    void InitSegments() {
        vertexHeights = new float[( segmentObject.xQuads + 1 ) * ( segmentObject.zQuads + 1 )];
        segments = new PlaneGen[3];
        for ( int i = 0; i < 3; i++ ) {
            PlaneGen newSegment;
            if ( i == 0 )
                newSegment = segmentObject;
            else
                newSegment = Instantiate( segmentObject );
            segments[i] = newSegment;

            newSegment.transform.SetParent( transform );
            newSegment.transform.localPosition = new Vector3( -newSegment.xSize * .5f, 0, newSegment.zSize * i );

            UpdateMeshForDistance( newSegment );
        }
    }

    void UpdateMeshForDistance( PlaneGen plane ) {
        int trackQuads = Mathf.RoundToInt( trackWidth / plane.xSize * plane.xQuads );
        int centreQuad = plane.xQuads / 2;
        int minTrackQuad = centreQuad - trackQuads / 2;
        int maxTrackQuad = centreQuad + trackQuads / 2 - ( 1 - trackQuads % 2 ) + 1;
        DebugExtras.Log( minTrackQuad + " " + maxTrackQuad );
        for ( int z = 0; z < plane.zQuads + 1; z++ ) {
            float zDist = distTravelled + plane.transform.localPosition.z + z * plane.quadYSize;
            for ( int x = 0; x < plane.xQuads + 1; x++ ) {
                if ( x >= minTrackQuad && x <= maxTrackQuad )
                    vertexHeights[z * ( plane.xQuads + 1 ) + x] = 0;
                else {
                    float xDist = x * plane.quadXSize - plane.xSize * .5f;
                    vertexHeights[z * ( plane.xQuads + 1 ) + x] = Mathf.PerlinNoise( xDist * perlinScale, zDist * perlinScale ) * maxGroundHeight;
                }
            }
        }
        plane.MakePlane( vertexHeights );
    }
}
