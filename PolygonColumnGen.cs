﻿/* Block.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MathUtils;
using System;

public class PolygonColumnGen : MeshGen {
    public Vector2[] polygonCorners;
    public float height;

    public void MakePolyColumn( Vector2[] surfacePoly, float height ) {
        DebugExtras.Assert( surfacePoly.Length >= 3 );
        this.polygonCorners = surfacePoly;
        this.height = height;
        GenMesh();
    }

    protected override void GeneratePoints( ref Vector3[] vertices, ref Vector2[] uvs, ref int[] triangles ) {
        int numVertices = polygonCorners.Length * 6;
        vertices = new Vector3[numVertices];
        uvs = new Vector2[numVertices];
        triangles = new int[12 * ( polygonCorners.Length - 1 )];

        for ( int i = 0; i < polygonCorners.Length; i++ ) {
            Vector3 polyCorner = polygonCorners[i].AsXZVector3();
            Vector3 nextPolyCorner = polygonCorners[( i + 1 ) % polygonCorners.Length].AsXZVector3();
            float uvSize = ( polyCorner - nextPolyCorner ).magnitude;
            int faceStartIndex = i * 6;

            // clockwise from bottom-right
            vertices[faceStartIndex] = polyCorner;
            uvs[faceStartIndex] = new Vector2( polyCorner.x, 0 );
            vertices[faceStartIndex + 1] = nextPolyCorner;
            uvs[faceStartIndex] = new Vector2( polyCorner.x - uvSize, 0 );
            vertices[faceStartIndex + 4] = polyCorner; // bottom cap

            polyCorner.y = height;
            nextPolyCorner.y = height;
            vertices[faceStartIndex + 2] = nextPolyCorner;
            uvs[faceStartIndex + 2] = new Vector2( polyCorner.x, height );
            vertices[faceStartIndex + 3] = polyCorner;
            uvs[faceStartIndex + 3] = new Vector2( polyCorner.x - uvSize, height );
            vertices[faceStartIndex + 5] = polyCorner; // top cap
            uvs[faceStartIndex + 4] = uvs[faceStartIndex + 5] = polygonCorners[i]; // cap uvs

            int triangleIndex = i * 6;
            triangles[triangleIndex] = faceStartIndex;
            triangles[triangleIndex + 1] = faceStartIndex + 2;
            triangles[triangleIndex + 2] = faceStartIndex + 1;
            triangles[triangleIndex + 3] = faceStartIndex;
            triangles[triangleIndex + 4] = faceStartIndex + 3;
            triangles[triangleIndex + 5] = faceStartIndex + 2;
        }

        // cap triangles
        for ( int i = 0; i < polygonCorners.Length - 2; i++ ) {
            int vertexIndex = polygonCorners.Length * 6 + i * 6;
            // bottom
            triangles[vertexIndex] = 4;
            triangles[vertexIndex + 1] = ( i + 1 ) * 6 + 4;
            triangles[vertexIndex + 2] = ( i + 2 ) * 6 + 4;

            // top
            triangles[vertexIndex + 3] = 5;
            triangles[vertexIndex + 4] = ( i + 2 ) * 6 + 5;
            triangles[vertexIndex + 5] = ( i + 1 ) * 6 + 5;
        }
    }
}