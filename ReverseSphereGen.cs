﻿/* PlaneGen.cs
 * Copyright Grasshopper 2013
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReverseSphereGen : MeshGen {
    public float radius;
    public int latitudes, longitudes;

    int[,][] vertexGroups;

    protected override void Start() {
        base.Start();

        GenMesh();
    }

    protected override void GeneratePoints(ref Vector3[] vertices, ref Vector2[] uvs, ref int[] triangles) {
        int numVertices = longitudes * ( latitudes - 1 ) + 2;
        vertices = new Vector3[numVertices];
        uvs = new Vector2[numVertices];
        triangles = new int[6 * longitudes * ( latitudes - 1 )];

        // add poles
        vertices[0] = Vector3.down * radius;
        vertices[numVertices - 1] = Vector3.up * radius;
        uvs[0] = Vector2.zero;
        uvs[numVertices - 1] = Vector2.up;

        for ( int latitude = 0; latitude < latitudes; latitude++ ) {
            int latStartVertIndex = 1 + latitude * longitudes;
            int latStartTriIndex = 3 * longitudes * ( 1 + 2 * ( latitude - 1 ) );
            float latRadians = Mathf.Asin( 2f * ( ( latitude + 1f ) / (float)latitudes - .5f ) ); 
            for ( int longitude = 0; longitude < longitudes; longitude++ ) {
                // vertices
                int vertIndex = latStartVertIndex + longitude;
                if ( latitude < latitudes - 1 ) {
                    // don't make new vertices for last latitude (goes to north pole)
                    float longRadians = longitude / (float)longitudes * Mathf.PI * 2f;
                    vertices[vertIndex] = GetSpherePoint( latRadians, longRadians );
                    Debug.Log( GetSpherePoint( latRadians, longRadians ) );

                }

                // triangles
                if ( latitude == 0 ) {
                    // south pole to first line of quads
                    triangles[longitude * 3] = 0;
                    triangles[longitude * 3 + 1] = 1 + ( longitude + 1 ) % longitudes;
                    triangles[longitude * 3 + 2] = longitude + 1;
                }
                else if ( latitude < latitudes - 1 ) {
                    // middle quads
                    var triStartIndex = latStartTriIndex + longitude * 6;
                    triangles[triStartIndex] = vertIndex;
                    int nextVertex = latStartVertIndex + ( longitude + 1 ) % longitudes;
                    triangles[triStartIndex + 1] = nextVertex - longitudes;
                    triangles[triStartIndex + 2] = nextVertex;
                    triangles[triStartIndex + 3] = vertIndex;
                    triangles[triStartIndex + 4] = vertIndex - longitudes;
                    triangles[triStartIndex + 5] = nextVertex - longitudes;
                }
                else {
                    // north cap
                    var triStartIndex = latStartTriIndex + longitude * 3;
                    triangles[triStartIndex] = vertIndex - longitudes;
                    triangles[triStartIndex + 1] = latStartVertIndex - longitudes + ( longitude + 1 ) % longitudes;
                    triangles[triStartIndex + 2] = numVertices - 1;
                }
            }
        }
    }

    Vector3 GetSpherePoint( float latRadians, float longRadians ) {
        latRadians = Mathf.PI * .5f - latRadians; // to angle from y axis
        return new Vector3( Mathf.Sin( latRadians ) * Mathf.Cos( longRadians ),
                           Mathf.Cos( latRadians ),
                           Mathf.Sin( latRadians ) * Mathf.Sin( longRadians ) ) 
            * radius;
    }

    public override void AddNormals( Mesh mesh, Vector3[] vertices, int[] triangles ) {
        var normals = new Vector3[vertices.Length];
        for ( int vertex = 0; vertex < normals.Length; vertex++ ) {
            normals[vertex] = -vertices[vertex] / radius;
        }
        mesh.normals = normals;
    }
}