﻿/* PlaneGen.cs
 * Copyright Grasshopper 2013
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class PlaneGen : MeshGen { 
    public int xQuads = 10;
    public int zQuads = 10;
    public float xSize = 10f;
    public float zSize = 10f;

    public float quadXSize { get; private set; }
    public float quadYSize { get; private set; }

    int[,][] vertexGroups;

    protected override void Awake() {
        base.Awake();

        quadXSize = xSize / xQuads;
        quadYSize = zSize / zQuads;
    }

    public void MakePlane( float[] vertexHeights ) {
        DebugExtras.Assert( vertexHeights.Length == ( xQuads + 1 ) * ( zQuads + 1 ) );

        Vector3[] vertices = null;
        Vector2[] uvs = null;
        int[] triangles = null;

        GeneratePoints( ref vertices, ref uvs, ref triangles );

        // put eights into mesh
        for ( int z = 0; z <= zQuads; z++ ) {
            for ( int x = 0; x <= xQuads; x++ ) {
                foreach ( var vertexIndex in GetVerticesAtQuadCorner( x, z ) ) {
                    vertices[vertexIndex].y = vertexHeights[z * ( xQuads + 1 ) + x];
                }
            }
        }

        GenMesh( vertices, uvs, triangles );
    }

    protected override void GeneratePoints(ref Vector3[] vertices, ref Vector2[] uvs, ref int[] triangles) {
        int numQuads = xQuads * zQuads;
        int numVertices = numQuads * 6;

        vertices = new Vector3[numVertices];
        uvs = new Vector2[numVertices];
        vertexGroups = new int[xQuads + 1, zQuads + 1][];
        var vertexGrouper = new List<int>();
        for ( int q = 0; q < numQuads; q++ )
        {
            int xQuad = q % xQuads;
            int zQuad = q / xQuads;
            Vector3 offset = new Vector3( xQuad * quadXSize, 0, zQuad * quadYSize );
            Vector2 uvOffset = new Vector2( offset.x, offset.z );
            vertices[q * 6] = offset;
            uvs[q * 6] = uvOffset;
            vertices[q * 6 + 1] = offset + new Vector3( 0, 0, quadYSize );
            uvs[q * 6 + 1] = uvOffset + new Vector2( 0, quadYSize );
            vertices[q * 6 + 2] = offset + new Vector3( quadXSize, 0, quadYSize );
            uvs[q * 6 + 2] = uvOffset + new Vector2( quadXSize, quadYSize );

            vertices[q * 6 + 3] = offset;
            uvs[q * 6 + 3] = uvOffset;
            vertices[q * 6 + 4] = offset + new Vector3( quadXSize, 0, quadYSize );
            uvs[q * 6 + 4] = uvOffset + new Vector2( quadXSize, quadYSize );
            vertices[q * 6 + 5] = offset + new Vector3( quadXSize, 0, 0 );
            uvs[q * 6 + 5] = uvOffset + new Vector2( quadXSize, 0 );

            // add corners points to changeable groups
            vertexGrouper.Clear();
            vertexGrouper.Add( q * 6 ); // add bottom left vertices
            vertexGrouper.Add( q * 6 + 3 );

            if ( xQuad > 0 )
                vertexGrouper.Add( q * 6 - 1 ); // add bottom right from last xQuad
            if ( zQuad > 0 ) {
                int bottomQuad = ( zQuad - 1 ) * xQuads + xQuad; // q of quad underneath this
                vertexGrouper.Add( bottomQuad * 6 + 1 ); // add upper left vertex from bottomQuad
                if ( xQuad > 0 ) {
                    vertexGrouper.Add( ( bottomQuad - 1 ) * 6 + 2 ); // add upper right vertices from bottom left quad
                    vertexGrouper.Add( ( bottomQuad - 1 ) * 6 + 4 );
                }
            }
            vertexGroups[xQuad, zQuad] = vertexGrouper.ToArray();

            if ( xQuad == xQuads - 1 ) {
                // add bottom right corner
                vertexGrouper.Clear();
                vertexGrouper.Add( q * 6 + 5 );
                if ( zQuad > 0 ) {
                    // add top right of quad below
                    int bottomQuad = ( zQuad - 1 ) * xQuads + xQuad; // q of quad underneath this
                    vertexGrouper.Add( bottomQuad * 6 + 2 );
                    vertexGrouper.Add( bottomQuad * 6 + 4 );
                }
                vertexGroups[xQuad + 1, zQuad] = vertexGrouper.ToArray();

                if ( zQuad == zQuads - 1 ) {
                    // add top right corner
                    vertexGrouper.Clear();
                    vertexGrouper.Add( q * 6 + 2 );
                    vertexGrouper.Add( q * 6 + 4 );
                    vertexGroups[xQuad + 1, zQuad + 1] = vertexGrouper.ToArray();
                }
            }

            if ( zQuad == zQuads - 1 ) {
                // add top left corner
                vertexGrouper.Clear();
                vertexGrouper.Add( q * 6 + 1 );
                if ( xQuad > 0 ) {
                    // add top right of previous quad
                    vertexGrouper.Add( ( q - 1 ) * 6 + 2 );
                    vertexGrouper.Add( ( q - 1 ) * 6 + 4 );
                }
                vertexGroups[xQuad, zQuad + 1] = vertexGrouper.ToArray();
            }
        }

        triangles = new int[numVertices];
        for ( int i = 0; i < triangles.Length; i++ )
            triangles[i] = i;
    }

    protected int[] GetVerticesAtQuadCorner( int xCorner, int zCorner ) {
        return vertexGroups[xCorner, zCorner];
    }

    public override void AddNormals( Mesh mesh, Vector3[] vertices, int[] triangles ) {
        var normals = new Vector3[triangles.Length];
        for ( int t = 0; t < normals.Length; t += 3 ) {
            Vector3 normal = Vector3.Cross( vertices[triangles[t + 1]] - vertices[triangles[t]]
                , vertices[triangles[t + 2]] - vertices[triangles[t]] ).normalized;

            normals[t] = normal;
            normals[t + 1] = normal;
            normals[t + 2] = normal;
        }
        mesh.normals = normals;
    }
}